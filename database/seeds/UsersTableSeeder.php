<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = Role::where('role', 'admin')->first();
        $dosenRole = Role::where('role', 'dosen')->first();
        $kaprodiRole = Role::where('role', 'kaprodi')->first();
        $wrRole = Role::where('role', 'wr')->first();

        User::create([
            'name' => 'Admin',
            'id_role' => $adminRole->id_role,
            'email' => 'admin@admin.com',
            'password' => bcrypt('admin'),
            'id_prodi' => 1,
            'username' => 'admin'
        ]);

        User::create([
            'name' => 'Dosen',
            'id_role' => $dosenRole->id_role,
            'email' => 'dosen@dosen.com',
            'password' => bcrypt('dosen'),
            'id_prodi' => 2,
            'username' => 'dosen'
        ]);

        User::create([
            'name' => 'Kaprodi',
            'id_role' => $kaprodiRole->id_role,
            'email' => 'kaprodi@kaprodi.com',
            'password' => bcrypt('kaprodi'),
            'id_prodi' => 2,
            'username' => 'kaprodi'
        ]);

        User::create([
            'name' => 'Wr',
            'id_role' => $wrRole->id_role,
            'email' => 'wr@wr.com',
            'password' => bcrypt('wr'),
            'id_prodi' => 3,
            'username' => 'wr'
        ]);
    }
}
