<?php

use App\Tahun;
use Illuminate\Database\Seeder;

class TahunTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tahun::create(['Tahun' => 2020]);
        Tahun::create(['Tahun' => 2021]);
        Tahun::create(['Tahun' => 2022]);
    }
}
