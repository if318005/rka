<?php

use App\Status;
use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Status::create(['Status' => 'Setuju']);
        Status::create(['Status' => 'Menunggu']);
        Status::create(['Status' => 'Ditolak']);
        Status::create(['Status' => 'Belum di kirim']);
    }
}
