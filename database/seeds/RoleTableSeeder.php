<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['role' => 'admin']);
        Role::create(['role' => 'dosen']);
        Role::create(['role' => 'kaprodi']);
        Role::create(['role' => 'wr']);
    }
}
