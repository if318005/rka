<?php

use App\Prodi;
use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class ProdiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Prodi::create(['nama_prodi' => 'D3 TI']);
        Prodi::create(['nama_prodi' => 'D3 TK']);
        Prodi::create(['nama_prodi' => 'D4 TRPL']);
        Prodi::create(['nama_prodi' => 'S1 IF']);
        Prodi::create(['nama_prodi' => 'S1 SI']);
        Prodi::create(['nama_prodi' => 'S1 TE']);
        Prodi::create(['nama_prodi' => 'S1 BP']);
        Prodi::create(['nama_prodi' => 'S1 MR']);
    }
}
