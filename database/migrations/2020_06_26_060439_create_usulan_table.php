<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsulanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usulan', function (Blueprint $table) {
            $table->bigIncrements('id_usulan');
            $table->integer('id_daftar')->unsigned();
            $table->string('rincianprogram');
            $table->integer('volume_usulan');
            $table->string('satuan_usulan');
            $table->integer('hargapersatuan');
            $table->string('keterangan');
            $table->integer('status_usulan')->unsigned();
            $table->string('komentar_usulan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usulan');
    }
}
