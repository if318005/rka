<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenggunaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penggunaan', function (Blueprint $table) {
            $table->bigIncrements('id_penggunaan');
            $table->integer('id_anggaran')->unsigned();
            $table->integer('rencana_biaya');
            $table->integer('realisai_biaya')->nullable();
            $table->date('tanggal');
            $table->string('file_usulan');
            $table->string('file_realisasi')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penggunaan');
    }
}
