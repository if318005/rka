<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDaftarRkaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daftar_rka', function (Blueprint $table) {
            $table->bigIncrements('id_daftar');
            $table->string('nama_rka');
            $table->integer('id_prodi')->unsigned();
            $table->integer('id_tahun')->unsigned();
            $table->integer('id_status')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daftar_rka');
    }
}
