<form action="/cobaText" method="POST" enctype="multipart/form-data">
    @csrf
    Tanggal    : <input type="date" name="tanggal"><br/>
    Jumlah     : <input type="number" name="bola"><br/>
    file usulan: <input type="file" name="file" accept="application/pdf"><br/>
    <button type="submit">Submit</button>
</form>
@php($a =1)
<br/><br/>
<table class="table table-hover" id="table-datatables">
    <thead>
    <tr>
        <th scope="col">No</th>
        <th scope="col">Tanggal</th>
        <th scope="col">Usulan Dana</th>
        <th scope="col">Realisasi</th>
        <th scope="col">Document Usulan</th>
        <th scope="col">Documet Realisasi</th>
        <th scope="col">Keterangan</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($penggunaans as $penggunaan)
        <tr>
            <td>{{$a++}}</td>
            <td>{{$penggunaan->tanggal}}</td>
            <td>Rp. {{$penggunaan->rencana_biaya}}</td>
            <td>
                @if (!$penggunaan->realisasi_biaya == null)
                    Rp. {{$penggunaan->realisasi_biaya}}
                @endif
            </td>
            <td><a href="{{url('/FileUsulan/' . $penggunaan->file_usulan)}}">Dokumen usulan</a></td>
            <td>
                @if (!$penggunaan->file_realisasi == null)
                    <a href="#">Dokumen realisasi</a>
                @endif
            </td>
            <td>
                @if ($penggunaan->status == 2 || $penggunaan->status == 3)
                    <a href="/kaprodi/editUsulan">
                        <button type="button" class="btn btn-primary">Edit</button>
                    </a>
                    <a href="#">
                        <button type="button" class="btn btn-danger" onclick="return confirm('Hapus usulan ini?');">Delete</button>
                    </a>

                @endif
                @if ($penggunaan->status == 1)
                    <a href="#" >
                        <button type="button" class="btn btn-info">Upload Realisasi</button>
                    </a>
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
