<!-- Logo -->
<?php
use App\Role;
use Illuminate\Support\Facades\Auth;
$role = Role::where('id_role', '=', Auth::user()->id_role)->first();
//echo $role;
?>
<a href="{{route('home')}}" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini">RKA</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>RKA IT del</b></span>
</a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    {{--  <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">--}}
    {{--    <span class="sr-only">Toggle navigation</span>--}}
    {{--    <span class="icon-bar"></span>--}}
    {{--    <span class="icon-bar"></span>--}}
    {{--    <span class="icon-bar"></span>--}}
    {{--  </a>--}}

    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <!-- Messages: style can be found in dropdown.less-->
            <!-- Tasks: style can be found in dropdown.less -->

            <!-- User Account: style can be found in dropdown.less -->
            @if (Auth::user()->id_role != 1 )
                <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                        <span class="label label-warning">{{Auth::user()->unreadNotifications->count()}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">Notifikasi</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu">
                                @if (Auth::user()->unreadNotifications->count() == 0)
                                    <li>
                                        <a href="#">
                                            Tidak ada pemberitahuan
                                        </a>
                                    </li>
                                @endif
                                @foreach(Auth::user()->unreadNotifications as $notifikasi)
                                    <li>
                                        <a href="{{$notifikasi->data['action']}}">
                                            {{$notifikasi->data['data']}}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                        <li class="footer"><a href="/notifikasi/asRead">Tandai telah di baca</a></li>
                    </ul>
                </li>
            @endif

            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
{{--                    <img src="" class="user-image" alt="User Image">--}}
                    <span class="hidden-xs">{{Auth::user()->username}}</span>&nbsp;<span class="glyphicon glyphicon-chevron-down"></span>
                </a>
                <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header">
                        <p>
                            {{Auth::user()->name}}
                            <small>{{$role->role}}</small>
                        </p>
                    </li>
                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <div class="pull-left">
                            <a href="/edit_user/{{Auth::user()->id}}" class="btn btn-default btn-flat menu-sidebar">Profile</a>
                        </div>
                        <div class="pull-right">
                            <a href="/keluar" class="btn btn-default btn-flat menu-sidebar">Logout</a>
                        </div>
                    </li>
                </ul>
            </li>

        </ul>
    </div>

</nav>
