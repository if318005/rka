<section class="sidebar">
    <!-- Sidebar user panel -->
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
        <li class="menu-sidebar"><a href="/home"><span class="fa fa-dashboard"></span> Beranda</span></a></li>
        @if(Auth::user()->id_role == 1)
            <li class="menu-sidebar"><a href="/tambah_user"><span class="glyphicon glyphicon-plus"></span> Registrasi</span></a></li>
        @elseif (Auth::user()->id_role == 2)
{{--            <li class="menu-sidebar"><a href="/dosen/daftarRequest"><span class="glyphicon glyphicon-tasks"></span> Daftar Request</span></a></li>--}}
        @elseif (Auth::user()->id_role == 3)
            <li class="menu-sidebar"><a href="/kaprodi/daftarRka"><span class="glyphicon glyphicon-th-list"></span> Daftar Rka</span></a></li>
            <li class="menu-sidebar"><a href="/kaprodi/tambahRka"><span class="glyphicon glyphicon-floppy-disk"></span> Request Rka</span></a></li>
            <li class="menu-sidebar"><a href="/kaprodi/daftarDana"><span class="glyphicon glyphicon-usd"></span> Laporan Dana</span></a></li>
        @elseif (Auth::user()->id_role == 4)
            <li class="menu-sidebar"><a href="/wr/daftarRka"><span class="glyphicon glyphicon-tasks"></span> Daftar RKA</span></a></li>
            <li class="menu-sidebar"><a href="/wr/daftarDana"><span class="glyphicon glyphicon-tasks"></span> Daftar Dana</span></a></li>
        @endif
{{--        @if(\Auth::user()->role == 1)--}}
{{--            <li class="menu-sidebar"><a href="#"><span class="fa fa-coffee"></span> Verifikasi</span></a></li>--}}

{{--            <li class="menu-sidebar"><a href="#"><span class="fa fa-exclamation"></span> Data Peserta</span></a></li>--}}

{{--        @endif--}}

{{--        <li class="header">OTHER</li>--}}

{{--        @if(\Auth::user()->name == 'Admin')--}}
{{--            <li class="menu-sidebar"><a href="#"><span class="glyphicon glyphicon-log-out"></span> Reset Password</span></a></li>--}}
{{--        @endif--}}

{{--        <li class="menu-sidebar"><a href="#"><span class="glyphicon glyphicon-log-out"></span> Logout</span></a></li>--}}


    </ul>
</section>
