@extends('dashboard.layouts.master')
<?php
use App\User;$users = User::all();
    $baris = 1;
?>
@section('content')
    @if(Auth::user()->id_role == 1)
        <div class="row">
            <div class="col-md-12">
                <h4></h4>
                <div class="box box-warning">
                    <div class="box-header">
                        <p>
{{--                            <button class="btn btn-sm btn-flat btn-warning btn-refresh"><i class="fa fa-refresh"></i> Refresh</button>--}}
                            <a href="/tambah_user">
                                <button type="button" class="btn btn-flat btn-primary btn-sm btn-success">+ Tambah User</button>
                            </a>

                        </p>
                    </div>
                    <div class="box-body">

                        <div class="table-responsive">
                            <table class="table table-hover" id="table-datatables">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Email</th>
                                    <th scope="col"></th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <th>{{$baris++}}</th>
                                        <th>{{$user->email}}</th>
                                        <th><a href="/edit_user/{{$user->id}}">
                                                <button type="button" class="btn btn-primary btn-sm">Edit</button>
                                            </a> </th>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    @endif

    @elseif(Auth::user()->id_role == 2)
        <div class="container">
            dosen
        </div>
    @elseif(Auth::user()->id_role == 3)
        <div class="container">
            kaprodi
        </div>
    @elseif(Auth::user()->id_role == 4)
        <div class="container">
            wr
        </div>
    @endif
@endsection

@section('scripts')

@endsection
