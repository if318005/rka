<?php

use App\DaftarRka;
use App\Tahun;
use App\User;
use App\Usulan;
use App\Prodi;
use App\Role;
use Illuminate\Support\Facades\DB;
$users = User::paginate(10);
$baris = 1;
$daftarRkas = DaftarRka::where('id_prodi', '=', Auth::user()->id_prodi)->latest()->get();;
$id_prodi = Auth::user()->id_prodi;
$daftar_Rka = DaftarRka::where('id_prodi', '=', $id_prodi)->latest()->get();
$tahun = Tahun::where('tahun', '=', date('Y'))->first();
$daftar_Rka_wr = DaftarRka::where('id_tahun', '=', $tahun->id_Tahun)->whereNotIn('id_status',[4])->get();
$usulans = Usulan::latest()->paginate(5);
?>
@extends('dashboard.layouts.master')

@section('content')
    @if(Auth::user()->id_role == 1)
        <div class="row">
            <div class="col-md-12">
                <h4></h4>
                <div class="box box-warning">
                    <div class="box-header">
{{--                        <p>--}}
{{--                            --}}{{--                            <button class="btn btn-sm btn-flat btn-warning btn-refresh"><i class="fa fa-refresh"></i> Refresh</button>--}}
{{--                            <a href="/tambah_user">--}}
{{--                                <button type="button" class="btn btn-flat btn-primary btn-sm btn-success">Registrasi User</button>--}}
{{--                            </a>--}}

{{--                        </p>--}}
                    </div>
                    <div class="box-body">
                        @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table table-hover" id="table-datatables">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Prodi</th>
                                    <th scope="col">Role</th>
                                    <th scope="col"></th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <th>{{$baris++}}</th>
                                        <th>{{$user->email}}</th>
                                        <th>{{Prodi::find($user->id_prodi)->nama_prodi}}</th>
                                        <th>{{Role::find($user->id_role)->role}}</th>
                                        <th>
{{--                                            <a href="/edit_user/{{$user->id}}">--}}
{{--                                                <button type="button" class="btn btn-primary btn-sm">Edit</button>--}}
{{--                                            </a>--}}
                                            <a href="/hapus_user/{{$user->id}}">
                                                <button type="button" class="btn btn-danger btn-sm" onclick="return confirm('Apakah anda ingin menghapus {{$user->email}}?')">Hapus</button>
                                            </a>
                                        </th>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{$users->links()}}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    @elseif(Auth::user()->id_role == 2)
        <div class="row">
            <div class="col-md-12">
                <h4></h4>
                <div class="box box-warning">
                    <div class="box-header">
                        <h4>Request RKA</h4>
                    </div>
                    <div class="box-body">
                        @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                        {{--                    <div class="alert alert-success">--}}
                        {{--                        Request dana berhasil!--}}
                        {{--                    </div>--}}
                        <form class="form-horizontal" method="POST" action="/dosen/requestRka" >
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="id_daftar" class="col-md-4 control-label">Daftar RKA</label>
                                <div class="col-md-6">
                                    <select class="form-control" name="id_daftar" id="id_daftar">
                                        @foreach ($daftarRkas as $daftarRka)
                                            <option value="{{$daftarRka->id_daftar}}">{{$daftarRka->nama_rka}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label id="deskripsi" for="deskripsi" class="col-md-4 control-label">Rincian Program</label>

                                <div class="col-md-6">
                                    <input type="text" id="deskripsi" name="deskripsi" class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="volume" class="col-md-4 control-label">Volume</label>

                                <div class="col-md-6">
                                    <input id="volume" type="number" class="form-control" name="volume" required>

                                </div>
                            </div>

                            <div class="form-group">
                                <label for="satuan" class="col-md-4 control-label">Satuan</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="satuan" id="satuan">
                                        <option value="Orang">Orang</option>
                                        <option value="Kegiatan">Kegiatan</option>
                                        <option value="Unit">Unit</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="harga" class="col-md-4 control-label">Harga Satuan</label>

                                <div class="col-md-6">
                                    <input id="harga" type="number" class="form-control" name="harga" required>

                                </div>
                            </div>

                            <div class="form-group">
                                <label for="keterangan" class="col-md-4 control-label">Keterangan</label>

                                <div class="col-md-6">
                                    <input id="keterangan" type="text" class="form-control" name="keterangan" required>

                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </form>

                        <br/><br/>
                            <table class="table table-hover" id="table-datatables">
                                <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Rincian Program</th>
                                    <th scope="col">Vol</th>
                                    <th scope="col">Satuan</th>
                                    <th scope="col">Harga Satuan</th>
                                    <th scope="col">Komentar</th>
                                    <th scope="col">Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($z = 1)
                                @foreach ($usulans as $usulan)
                                    @if ($usulan->status_usulan == 1)
                                        <tr class="success">
                                            <td>{{$z++}}</td>
                                            <td>{{$usulan->rincianprogram}}</td>
                                            <td>{{$usulan->volume_usulan}}</td>
                                            <td>{{$usulan->satuan_usulan}}</td>
                                            <td>{{$usulan->hargapersatuan}}</td>
                                            <td>{{$usulan->komentar_usulan}}</td>
                                            <td>Di terima</td>
                                        </tr>
                                    @elseif($usulan->status_usulan == 2)
                                        <tr>
                                            <td>{{$z++}}</td>
                                            <td>{{$usulan->rincianprogram}}</td>
                                            <td>{{$usulan->volume_usulan}}</td>
                                            <td>{{$usulan->satuan_usulan}}</td>
                                            <td>{{$usulan->hargapersatuan}}</td>
                                            <td>{{$usulan->komentar_usulan}}</td>
                                            <td>Menunggu</td>
                                        </tr>
                                    @elseif($usulan->status_usulan == 3)
                                        <tr class="warning">
                                            <td>{{$z++}}</td>
                                            <td>{{$usulan->rincianprogram}}</td>
                                            <td>{{$usulan->volume_usulan}}</td>
                                            <td>{{$usulan->satuan_usulan}}</td>
                                            <td>{{$usulan->hargapersatuan}}</td>
                                            <td>{{$usulan->komentar_usulan}}</td>
                                            <td>Di tolak</td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                            {{$usulans->links()}}
                    </div>
                </div>
            </div>
        </div>
    @elseif(Auth::user()->id_role == 3)
        <div class="row">
            <div class="col-md-12">
                <h4></h4>
                <div class="box box-warning">
                    <div class="box-header">
                    </div>
                    <div class="box-body">

                        <div class="table-responsive">
                            <table class="table table-hover" id="table-datatables">
                                <thead>
                                <tr>
                                    <th scope="col"></th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($daftar_Rka as $rka)
                                    <tr>
                                        <td><a href="kaprodi/daftarrka/{{$rka->id_daftar}}">{{$rka->nama_rka}}</a> </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    @elseif(Auth::user()->id_role == 4)
        <div class="row">
            <div class="col-md-12">
                <h4></h4>
                <div class="box box-warning">
                    <div class="box-header">
                    </div>
                    <div class="box-body">

                        <div class="table-responsive">
                            <table class="table table-hover" id="table-datatables">
                                <thead>
                                <tr>
                                    <th scope="col"></th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($daftar_Rka_wr as $rka)
                                    <tr>
                                        <td><a href="/wr/rka/{{$rka->id_daftar}}">{{$rka->nama_rka}}</a> </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    @endif

@endsection
@section('scripts')

@endsection
