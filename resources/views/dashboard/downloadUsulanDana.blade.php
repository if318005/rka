<!DOCTYPE html>
<html>
<head>
    <title>Daftar Rka</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<style type="text/css">
    table tr td,
    table tr th{
        font-size: 9pt;
    }
</style>
<center>
    <h5>{{$judul}}</h5>
</center>
<br>

<table class='table table-bordered'>
    <thead>
    <tr>
        <th scope="col">Mata Anggaran</th>
        <th scope="col">Kegiatan/Aktivitas</th>
        <th scope="col">Anggaran Biaya</th>
        <th scope="col">Realisasi Biaya</th>
        <th scope="col">Sisa</th>
    </tr>
    </thead>
    <tbody>
    @php($a = 0)
    @foreach ($anggarans as $anggaran)
        <tr>
            <td>{{$anggaran->mata_anggaran}}</td>
            <td>{{$anggaran->rincian_program}}</td>
            <td>{{$anggaran->volume * $anggaran->harga_persatuan}}</td>
            <td>{{$realisasi[$a]}}</td>
            <td>{{$anggaran->volume * $anggaran->harga_persatuan - $realisasi[$a++]}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
<br/><br/>
<div class="align-items-center">
    <div class="col-md-8">
        <div>Total Dana      : Rp {{$totalDana}}</div>
        <div>Total realisasi : Rp {{$totalRealisasi}}</div>
        <div>Total sisa      :Rp {{$sisa}}</div>
    </div>
    <div class="col-md-3">
        <center>
            <p>Laguboti,</p>
            <p><?= date("l, d-m-Y") ?></p>
            <br/><br/>
            <p>___________________</p>
        </center>
    </div>
</div>
</body>
</html>
