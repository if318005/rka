@extends('dashboard.layouts.master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h4></h4>
            <div class="box box-warning">
                <div class="box-header">
                </div>
                <div class="box-body">

                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    <table class="table table-hover" id="table-datatables">
                        <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Rincian Program</th>
                            <th scope="col">Vol</th>
                            <th scope="col">Satuan</th>
                            <th scope="col">Harga Satuan</th>
                            <th scope="col">Komentar</th>
                            <th scope="col">Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php($a = 1)
                        @foreach ($usulans as $usulan)
                            @if ($usulan->status_usulan == 1)
                                <tr class="success">
                                    <td>{{$a++}}</td>
                                    <td>{{$usulan->rincianprogram}}</td>
                                    <td>{{$usulan->volume_usulan}}</td>
                                    <td>{{$usulan->satuan_usulan}}</td>
                                    <td>{{$usulan->hargapersatuan}}</td>
                                    <td>{{$usulan->komentar_usulan}}</td>
                                    <td>Di terima</td>
                                </tr>
                            @elseif($usulan->status_usulan == 2)
                                <tr>
                                    <td>{{$a++}}</td>
                                    <td>{{$usulan->rincianprogram}}</td>
                                    <td>{{$usulan->volume_usulan}}</td>
                                    <td>{{$usulan->satuan_usulan}}</td>
                                    <td>{{$usulan->hargapersatuan}}</td>
                                    <td>{{$usulan->komentar_usulan}}</td>
                                    <td>Menunggu</td>
                                </tr>
                            @elseif($usulan->status_usulan == 3)
                                <tr class="warning">
                                    <td>{{$a++}}</td>
                                    <td>{{$usulan->rincianprogram}}</td>
                                    <td>{{$usulan->volume_usulan}}</td>
                                    <td>{{$usulan->satuan_usulan}}</td>
                                    <td>{{$usulan->hargapersatuan}}</td>
                                    <td>{{$usulan->komentar_usulan}}</td>
                                    <td>Di tolak</td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                    {{$usulans->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection

