@extends('dashboard.layouts.master')
@section('content')


    <div class="row">
        <div class="col-md-12">
            <h4></h4>
            <div class="box box-warning">
                <div class="box-header">
                    <div class="row">
                        <div>
                            <center>
                                <h3>{{$judul}}</h3>
                            </center>
                        </div>
                    </div>
                </div>
                <div class="box-body">
                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-md-10">

                        </div>
                        <div class="col-md-2">
                            <a href="/usulan/pdf/{{$id_daftar}}"><button class="btn btn-primary">Download Document</button> </a>
                        </div>
                    </div>
                    <table class="table table-hover" id="table-datatables">
                        <thead>
                        <tr>
                            <th scope="col">Mata Anggaran</th>
                            <th scope="col">Kegiatan/Aktivitas</th>
                            <th scope="col">Anggaran Biaya</th>
                            <th scope="col">Realisasi Biaya</th>
                            <th scope="col">Sisa</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php($a = 0)
                        @foreach ($anggarans as $anggaran)
                            <tr>
                                <td>{{$anggaran->mata_anggaran}}</td>
                                <td>{{$anggaran->rincian_program}}</td>
                                <td>{{$anggaran->volume * $anggaran->harga_persatuan}}</td>
                                <td>{{$realisasi[$a]}}</td>
                                <td>{{$anggaran->volume * $anggaran->harga_persatuan - $realisasi[$a++]}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <br/>
                    <div class="row">
                        <div class="col-md-2">
                            <p>Total Dana</p>
                        </div>
                        <div class="col-md-10">
                            : Rp {{$totalDana}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <p>Total realisasi</p>
                        </div>
                        <div class="col-md-10">
                            <p>: Rp {{$totalRealisasi}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <p>Total sisa</p>
                        </div>
                        <div class="col-md-10">
                            <p>: Rp {{$sisa}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection

