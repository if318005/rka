@extends('dashboard.layouts.master')
@section('content')


    <div class="row">
        <div class="col-md-12">
            <h4></h4>
            <div class="box box-warning">
                <div class="box-header">
                    <div class="row">
                        <div class="col-md-2">
                            <p>Mata Anggaran</p>
                        </div>
                        <div class="col-md-10">
                            <p>: {{$anggaran->mata_anggaran}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <p>Kegiatan</p>
                        </div>
                        <div class="col-md-10">
                            <p>: {{$anggaran->rincian_program}}</p>
                        </div>
                    </div>
                </div>
                <div class="box-body">
{{--                    <div class="alert alert-success">--}}
{{--                        Usulan dana di terima!--}}
{{--                    </div>--}}
{{--                    <div class="alert alert-success">--}}
{{--                        Usulan dana di tolak!--}}
{{--                    </div>--}}
                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    <table class="table table-hover" id="table-datatables">
                        @php($a = 1)
                        @php($jumlah = 0)
                        <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Tanggal</th>
                            <th scope="col">Usulan Dana</th>
                            <th scope="col">Realisasi</th>
                            <th scope="col">Document Usulan</th>
                            <th scope="col">Document Realisasi</th>
                            <th scope="col">Keterangan</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($penggunaans as $penggunaan)
                            <tr>
                                <td>{{$a++}}</td>
                                <td>{{$penggunaan->tanggal}}</td>
                                <td>Rp. {{$penggunaan->rencana_biaya}}@php($jumlah = $jumlah + $penggunaan->rencana_biaya)</td>
                                <td>
                                    @if (!$penggunaan->realisasi_biaya == null)
                                        Rp. {{$penggunaan->realisai_biaya}}
                                    @endif
                                </td>
                                <td><a href="{{url('/FileUsulan/' . $penggunaan->file_usulan)}}">Dokumen usulan</a></td>
                                <td>
                                    @if (!$penggunaan->file_realisasi == null)
                                        <a href="#">Dokumen realisasi</a>
                                    @endif
                                </td>
                                <td>
                                    @if ($penggunaan->status == 2 )
                                        <a href="/wr/usulan/terima/{{$penggunaan->id_penggunaan}}">
                                            <button type="button" class="btn btn-success">Terima</button>
                                        </a>
                                        <a href="/wr/usulan/tolak/{{$penggunaan->id_penggunaan}}">
                                            <button type="button" class="btn btn-danger" >Tolak</button>
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <br/>
                    <div class="row">
                        <div class="col-md-2">
                            <p>sisa</p>
                        </div>
                        <div class="col-md-10">
                            <p>: Rp {{($anggaran->harga_persatuan*$anggaran->volume) - $jumlah}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection

