@extends('dashboard.layouts.master')
<?php
use App\Anggaran;

$a = 1;
$rkas = Anggaran::where('id_daftar', '=', $daftarRka->id_daftar)->get();
?>
@section('content')


    <div class="row">
        <div class="col-md-12">
            <h4></h4>
            <div class="box box-warning">
                <div class="box-header">
                    <b>{{$daftarRka->nama_rka}}</b>
                </div>
                <div class="box-body">
                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    @if ($daftarRka->id_status == 1)
                        <a href="/rka/pdf/{{$daftarRka->id_daftar}}">
                            <button class="float-right btn btn-primary">Dowload document</button>
                        </a>
                    @endif
                    <table class="table table-hover" id="table-datatables">
                        <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Mata Anggaran</th>
                            <th scope="col">Rincian Program</th>
                            <th scope="col">Vol</th>
                            <th scope="col">Satuan</th>
                            <th scope="col">Harga Satuan</th>
                            <th scope="col">Jumlah</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($rkas as $rka)
                            <tr>
                                <td>{{$a++}}</td>
                                <td>
                                    @if ($daftarRka->id_status == 1)
                                        <a href="/wr/daftarUsulan/{{$rka->id_anggaran}}">{{$rka->mata_anggaran}}</a>
                                    @else()
                                        {{$rka->mata_anggaran}}
                                    @endif
                                </td>
                                <td>{{$rka->rincian_program}}</td>
                                <td>{{$rka->volume}}</td>
                                <td>{{$rka->satuan}}</td>
                                <td>{{$rka->harga_persatuan}}</td>
                                <td>{{$rka->volume*$rka->harga_persatuan}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @if ($daftarRka->id_status == 2)
                        <div>
                            <br>
                            <a href="/wr/terima/{{$daftarRka->id_daftar}}">
                                <button class="float-right btn btn-success">Terima</button>
                            </a>
                            <a href="/wr/tolak/{{$daftarRka->id_daftar}}">
                                <button class="float-right btn btn-danger">Tolak</button>
                            </a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection

