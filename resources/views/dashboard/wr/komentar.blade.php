@extends('dashboard.layouts.master')
<?php
use App\Anggaran;use App\DaftarRka;

$a = 1;
$rkas = Anggaran::where('id_daftar', '=', $daftarRka->id_daftar)->get();
?>
@section('content')


    <div class="row">
        <div class="col-md-12">
            <h4></h4>
            <div class="box box-warning">
                <div class="box-header">
                    <b>{{$daftarRka->nama_rka}}</b>
                </div>
                <div class="box-body">
                    <form method="POST" action="/wr/komentar">
                        @csrf
    {{--                    <div class="alert alert-success">--}}
    {{--                        Daftar RKA di tolak!--}}
    {{--                    </div>--}}
    {{--                    <div class="alert alert-success">--}}
    {{--                        Komentar dikirim!--}}
    {{--                    </div>--}}
                        @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                        @if ($daftarRka->id_status == 1)
                            <a href="/rka/pdf/{{$daftarRka->id_daftar}}">
                                <button class="float-right btn btn-primary">Dowload document</button>
                            </a>
                        @endif
                        <input type="hidden" value="{{$daftarRka->id_daftar}}" name="id_daftar">
                            <table class="table table-hover" id="table-datatables">
                                <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Mata Anggaran</th>
                                    <th scope="col">Rincian Program</th>
                                    <th scope="col">Vol</th>
                                    <th scope="col">Satuan</th>
                                    <th scope="col">Harga Satuan</th>
                                    <th scope="col">Jumlah</th>
                                    <th scope="col">Komentar</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($rkas as $rka)
                                    <tr>
                                        <td>{{$a++}}</td>
                                        <td>{{$rka->mata_anggaran}}</td>
                                        <td>{{$rka->rincian_program}}</td>
                                        <td>{{$rka->volume}}</td>
                                        <td>{{$rka->satuan}}</td>
                                        <td>{{$rka->harga_persatuan}}</td>
                                        <td>{{$rka->volume*$rka->harga_persatuan}}</td>
                                        <td>
                                            <textarea name="komentar[]"></textarea>
                                            <input type="hidden" name="id_anggaran[]" value="{{$rka->id_anggaran}}">
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        <br>
                        <div class="row">
                            <div class="col-md-10">

                            </div>
                            <div class="col-md-2">
                                <input type="submit" name="submit" class="btn btn-primary" value="Kirim">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection

