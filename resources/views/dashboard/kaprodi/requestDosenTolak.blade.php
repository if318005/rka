@extends('dashboard.layouts.master')
@section('content')


    <div class="row">
        <div class="col-md-12">
            <h4></h4>
            <div class="box box-warning">
                <div class="box-header">
                    {{--                    <h4>Edit User</h4>--}}
                </div>
                <div class="box-body">

                    <form class="form-horizontal" method="POST" action="/kaprodi/lihatRequest/tolak">
                        {{ csrf_field() }}

                        <input type="hidden" name="id_usulan" value="{{$usulan->id_usulan}}">

                        <div class="form-group">
                            <label for="rincian" class="col-md-4 control-label">Rincian Program</label>

                            <div class="col-md-6">
                                <input type="text" id="rincian" class="form-control" readonly value="{{$usulan->rincianprogram}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="volume" class="col-md-4 control-label">Volume</label>

                            <div class="col-md-6">
                                <input type="number" id="volume" class="form-control" readonly value="{{$usulan->volume_usulan}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="satuan" class="col-md-4 control-label">Satuan</label>

                            <div class="col-md-6">
                                <input type="text" id="satun" class="form-control" readonly value="{{$usulan->satuan_usulan}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="harga" class="col-md-4 control-label">Harga Persatuan</label>

                            <div class="col-md-6">
                                <input type="text" id="harga" class="form-control" readonly value="{{$usulan->hargapersatuan}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="keterangan" class="col-md-4 control-label">Keterangan</label>

                            <div class="col-md-6">
                                <input type="text" id="keterangan" class="form-control" readonly value="{{$usulan->keterangan}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="komentar" class="col-md-4 control-label">Komentar</label>

                            <div class="col-md-6">
                                <input type="text" id="komentar" class="form-control" name="komentar">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection
