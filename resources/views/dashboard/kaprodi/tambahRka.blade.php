@extends('dashboard.layouts.master')

@section('content')


    <div class="row">
        <div class="col-md-12">
            <h4></h4>
            <div class="box box-warning">
                <div class="box-header">
                    {{--                    <h4>Edit User</h4>--}}
                </div>
                <div class="box-body">
                    <form class="form-horizontal" method="POST" action="/kaprodi/tambahRka">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="id_daftar" class="col-md-4 control-label">Daftar RKA</label>
                            <div class="col-md-6">
                                <select class="form-control" name="id_daftar" id="id_daftar">
                                    @foreach ($daftarRkas as $daftarRka)
                                        <option value="{{$daftarRka->id_daftar}}">{{$daftarRka->nama_rka}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="mata_anggaran" class="col-md-4 control-label">Mata Anggaran</label>

                            <div class="col-md-6">
                                <input id="mata_anggaran" type="text" class="form-control" name="mata_anggaran" required>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="rincian_program" class="col-md-4 control-label">Rincian Program</label>

                            <div class="col-md-6">
                                <input id="rincian_program" type="text" class="form-control" name="rincian_program" required>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="volume" class="col-md-4 control-label">Volume</label>

                            <div class="col-md-6">
                                <input id="volume" type="number" class="form-control" name="volume" required>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="satuan" class="col-md-4 control-label">Satuan</label>

                            <div class="col-md-6">
                                <select class="form-control" name="satuan" id="satuan">
                                    <option value="Orang">Orang</option>
                                    <option value="Kegiatan">Kegiatan</option>
                                    <option value="Unit">Unit</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="harga" class="col-md-4 control-label">Harga Satuan</label>

                            <div class="col-md-6">
                                <input id="harga" type="number" class="form-control" name="harga" required>

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Kirim
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection
