{{--{{request()->get('tahun')}}--}}
@extends('dashboard.layouts.master')
<?php
    use App\Komentar;
//use App\Anggaran;use App\DaftarRka;
//use App\Status;use Illuminate\Support\Facades\Auth;
//$exists = DaftarRka::where('id_prodi', '=', Auth::user()->id_prodi)->where('id_tahun', '=', request()->get('tahun'))->exists();
//if ($exists == true){
////    $idDaftar = DaftarRka::get('id_daftar')->where('id_prodi', '=', Auth::user()->id_prodi)->where('id_tahun', '=', request()->get('tahun'));
//    $daftarRka = DaftarRka::where('id_prodi', '=', Auth::user()->id_prodi)->where('id_tahun', '=', request()->get('tahun'))->first();
////    echo $daftarRka;
//    $status = Status::where('id_status', '=', $daftarRka->id_status)->get('status')->first();
//}
//$exists2 = Anggaran::where('id_daftar', '=', $daftarRka->id_daftar)->exists();
//if($exists2 == true){
//    $rkas = Anggaran::where('id_daftar', '=', $daftarRka->id_daftar)->get();
//}
$a = 1;
?>
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h4></h4>
            <div class="box box-warning">
                <div class="box-header">
{{--                    @if($exists == 0)--}}
{{--                        <a href="/kaprodi/tambahDaftarRka/{{request()->get('tahun')}}">--}}
{{--                            <button type="button" class="btn btn-primary">Buat Daftar RKA</button>--}}
{{--                        </a>--}}
{{--                    @else--}}
{{--                    @endif--}}
                </div>
                <div class="box-body">
                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    @if($exists == 0)
                        @if ($daftarRka->id_status == 1)
                            <div class="row">
                                <div class="col-md-9">

                                </div>
                                <div class="col-md-3">
                                    <a href="/rka/pdf/{{$daftarRka->id_daftar}}">
                                        <button class="float-right btn btn-primary">Dowload document</button>
                                    </a>
                                </div>
                            </div>
                        @endif
                    @endif
                    @if($exists != 0)
                        <p><b>status : {{$status->status}}</b></p>
                    @endif
{{--                    @if ($exists2 != 0)--}}
                            <table class="table table-hover" id="table-datatables">
                                <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Mata Anggaran</th>
                                    <th scope="col">Rincian Program</th>
                                    <th scope="col">Vol</th>
                                    <th scope="col">Satuan</th>
                                    <th scope="col">Harga Satuan</th>
                                    <th scope="col">Jumlah</th>
                                    @if ($daftarRka->id_status == 3 || $daftarRka->id_status == 4)
                                        <th scope="col">Komentar</th>
                                        <th scope="col"></th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($rkas as $rka)
                                    <tr>
                                        <td>{{$a++}}</td>
                                        <td>
                                            @if ($daftarRka->id_status == 1)
                                                <a href="/kaprodi/kelolaDana/{{$rka->id_anggaran}}">{{$rka->mata_anggaran}}</a>
                                            @else
                                                {{$rka->mata_anggaran}}
                                            @endif
                                        </td>
                                        <td>{{$rka->rincian_program}}</td>
                                        <td>{{$rka->volume}}</td>
                                        <td>{{$rka->satuan}}</td>
                                        <td>{{$rka->harga_persatuan}}</td>
                                        <td>{{$rka->volume*$rka->harga_persatuan}}</td>
                                        @if ($daftarRka->id_status == 3 || $daftarRka->id_status == 4)
                                            <td>
                                                @if (Komentar::where('id_anggaran', '=', $rka->id_anggaran)->exists())
                                                    {{Komentar::where('id_anggaran', '=', $rka->id_anggaran)->latest()->first()->komentar}}
                                                @endif
                                            </td>
                                            <td>
                                                <a href="/kaprodi/editrka/{{$rka->id_anggaran}}">
                                                    <button type="button" class="btn btn-primary">Edit</button>
                                                </a>
                                                <a href="/kaprodi/hapusrka/{{$rka->id_anggaran}}" onclick="return confirm('Apakah anda ingin menghapus RKA ini?');">
                                                    <button type="button" class="btn btn-danger">Hapus</button>
                                                </a>
                                            </td>
                                        @endif

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @if ($daftarRka->id_status == 3 || $daftarRka->id_status == 4)
                            <a href="/kaprodi/kirim/{{$daftarRka->id_daftar}}">
                                <button class="float-right btn btn-secondary">Kirim</button>
                            </a>
                        @endif
{{--                    @endif--}}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection

