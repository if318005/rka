@extends('dashboard.layouts.master')
@section('content')
<?php
    $a = 1;
?>

    <div class="row">
        <div class="col-md-12">
            <h4></h4>
            <div class="box box-warning">
                <div class="box-header">
                    <div class="row">
                        <div class="col-md-2">
                            <p>Mata Anggaran</p>
                        </div>
                        <div class="col-md-10">
                            <p>: {{$anggaran->mata_anggaran}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <p>Kegiatan</p>
                        </div>
                        <div class="col-md-10">
                            <p>: {{$anggaran->rincian_program}}</p>
                        </div>
                    </div>
                </div>
                <div class="box-body">
{{--                    <div class="alert alert-success">--}}
{{--                        Usulan telah di ubah!--}}
{{--                    </div>--}}
{{--                    <div class="alert alert-success">--}}
{{--                        Realisasi telah di tambahkan!--}}
{{--                    </div>--}}
{{--                    <div class="alert alert-success">--}}
{{--                        Usulan di hapus!--}}
{{--                    </div>--}}
                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-md-12">
                            <a href="/kaprodi/requestDana/{{$anggaran->id_anggaran}}">
                                <button class="btn btn-primary">Tambah Usulan Dana</button>
                            </a>
                        </div>
                    </div>
                    <br/>
                    <table class="table table-hover" id="table-datatables">
                        <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Tanggal</th>
                            <th scope="col">PIC</th>
                            <th scope="col">Usulan Dana</th>
                            <th scope="col">Realisasi</th>
                            <th scope="col">Document Usulan</th>
                            <th scope="col">Documet Realisasi</th>
                            <th scope="col">Keterangan</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($penggunaans as $penggunaan)
                            <tr>
                                <td>{{$a++}}</td>
                                <td>{{$penggunaan->tanggal}}</td>
                                <td>{{$penggunaan->penanggung_jawab}}</td>
                                <td>Rp. {{$penggunaan->rencana_biaya}}</td>
                                <td>
                                    @if (!$penggunaan->realisai_biaya == null)
                                        Rp. {{$penggunaan->realisai_biaya}}
                                    @endif
                                </td>
                                <td><a href="{{url('/FileUsulan/' . $penggunaan->file_usulan)}}">Dokumen usulan</a></td>
                                <td>
                                    @if (!$penggunaan->file_realisasi == null)
                                        <a href="{{url('/FileRealisasi/' . $penggunaan->file_usulan)}}">Dokumen realisasi</a>
                                    @endif
                                </td>
                                <td>
                                    @if ($penggunaan->status == 2 || $penggunaan->status == 3)
                                        <a href="/kaprodi/editUsulan/{{$penggunaan->id_penggunaan}}">
                                            <button type="button" class="btn btn-primary">Edit</button>
                                        </a>
                                        <a href="/kaprodi/hapusUsulan/{{$penggunaan->id_penggunaan}}">
                                            <button type="button" class="btn btn-danger" onclick="return confirm('Hapus usulan ini?');">Delete</button>
                                        </a>

                                    @endif
                                    @if ($penggunaan->status == 1)
                                        <a href="/kaprodi/updateRealisasi/{{$penggunaan->id_penggunaan}}" >
                                            <button type="button" class="btn btn-info">Upload Realisasi</button>
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{$penggunaans->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection

