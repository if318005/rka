@extends('dashboard.layouts.master')
<?php
use App\Prodi;use App\Tahun;
$tahuns = Tahun::all();
$prodis = Prodi::all();
?>
@section('content')


    <div class="row">
        <div class="col-md-12">
            <h4></h4>
            <div class="box box-warning">
                <div class="box-header">
                    {{--                    <h4>Edit User</h4>--}}
                </div>
                <div class="box-body">

                    <form class="form-horizontal" method="POST" action="/kaprodi/daftarDana">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="tahun" class="col-md-4 control-label">Tahun</label>

                            <div class="col-md-6">
                                <select class="form-control" name="tahun" id="tahun">
                                    @foreach($tahuns as $tahun)
                                        <option value='{{$tahun->id_Tahun}}'>{{$tahun->tahun}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Cari
                                </button>
                            </div>
                        </div>
                    </form>
                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection
