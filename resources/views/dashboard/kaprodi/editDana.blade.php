@extends('dashboard.layouts.master')

@section('content')


    <div class="row">
        <div class="col-md-12">
            <h4></h4>
            <div class="box box-warning">
                <div class="box-header">
                    {{--                    <h4>Edit User</h4>--}}
                </div>
                <div class="box-body">
                    <form class="form-horizontal" method="POST" action="">
                        {{ csrf_field() }}

                        <input type="hidden" name="id_anggaran" value="{{request()->id}}">

                        <div class="form-group">
                            <label for="mata_anggaran" class="col-md-4 control-label">Mata Anggaran</label>

                            <div class="col-md-6">
                                <input type="text" value="A.II.2.2" readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="volume" class="col-md-4 control-label">Tanggal</label>

                            <div class="col-md-6">
                                <input id="volume" type="date" class="form-control"  name="volume" required>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="harga" class="col-md-4 control-label">Jumlah</label>

                            <div class="col-md-6">
                                <input id="harga" type="number" value="15000" class="form-control" name="harga" required>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="bukti" class="col-md-4 control-label">Upload Bukti</label>

                            <div class="col-md-6">
                                <input id="bukti" type="file" class="form-control" name="bukti" required>

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection
