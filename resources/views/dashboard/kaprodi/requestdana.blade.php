@extends('dashboard.layouts.master')

@section('content')


    <div class="row">
        <div class="col-md-12">
            <h4></h4>
            <div class="box box-warning">
                <div class="box-header">
                    {{--                    <h4>Edit User</h4>--}}
                </div>
                <div class="box-body">
                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
{{--                    <div class="alert alert-success">--}}
{{--                        Request dana berhasil!--}}
{{--                    </div>--}}
                    <form class="form-horizontal" method="POST" action="/kaprodi/requestDana" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <input type="hidden" name="id_anggaran" value="{{$anggaran->id_anggaran}}">

                        <div class="form-group">
                            <label for="mata_anggaran" class="col-md-4 control-label">Mata Anggaran</label>

                            <div class="col-md-6">
                                <input type="text" value="{{$anggaran->mata_anggaran}}" class="form-control" readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="tanggal" class="col-md-4 control-label">Tanggal</label>

                            <div class="col-md-6">
                                <input id="tanggal" type="date" class="form-control"  name="tanggal" required>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="penganggung_jawab" class="col-md-4 control-label">PIC</label>

                            <div class="col-md-6">
                                <input id="Penanggung_jawab" type="text" class="form-control"  name="penanggung_jawab" required>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="jumlah" class="col-md-4 control-label">Jumlah</label>

                            <div class="col-md-6">
                                <input id="jumlah" type="number" class="form-control" name="jumlah" required>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="bukti" class="col-md-4 control-label">Upload Bukti</label>

                            <div class="col-md-6">
                                <input id="bukti" type="file" class="form-control" name="bukti" accept="application/pdf" required>

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection
