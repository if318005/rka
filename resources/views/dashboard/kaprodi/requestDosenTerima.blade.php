@extends('dashboard.layouts.master')

@section('content')


    <div class="row">
        <div class="col-md-12">
            <h4></h4>
            <div class="box box-warning">
                <div class="box-header">
                    {{--                    <h4>Edit User</h4>--}}
                </div>
                <div class="box-body">
                    <form class="form-horizontal" method="POST" action="/kaprodi/lihatRequest/terima">
                        {{ csrf_field() }}

                        <input type="hidden" value="{{$usulan->id_daftar}}" name="id_daftar">
                        <input type="hidden" value="{{$usulan->id_usulan}}" name="id_usulan">

                        <div class="form-group">
                            <label for="mata_anggaran" class="col-md-4 control-label">Mata Anggaran</label>

                            <div class="col-md-6">
                                <input id="mata_anggaran" type="text" class="form-control" name="mata_anggaran" required>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="rincian_program" class="col-md-4 control-label">Rincian Program</label>

                            <div class="col-md-6">
                                <input id="rincian_program" type="text" readonly="readonly" class="form-control" value="{{$usulan->rincianprogram}}" name="rincian_program" required>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="volume" class="col-md-4 control-label">Volume</label>

                            <div class="col-md-6">
                                <input id="volume" type="number" class="form-control" readonly="readonly" value="{{$usulan->volume_usulan}}" name="volume" required>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="satuan" class="col-md-4 control-label">Satuan</label>

                            <div class="col-md-6">
                                <input type="text" id="satuan" class="form-control" readonly="readonly" value="{{$usulan->satuan_usulan}}" name="satuan">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="harga" class="col-md-4 control-label">Harga Satuan</label>

                            <div class="col-md-6">
                                <input id="harga" type="number" class="form-control" readonly="readonly" value="{{$usulan->hargapersatuan}}" name="harga" required>

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection
