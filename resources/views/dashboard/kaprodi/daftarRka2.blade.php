@extends('dashboard.layouts.master')
<?php
use App\DaftarRka;
use App\Status;
use App\Komentar;

$a = 1;
$stat = DaftarRka::where('id_daftar', '=', $id_daftar)->get('id_status')->first();
$status = Status::where('id_status', '=', $stat->id_status)->first();
?>
@section('content')


    <div class="row">
        <div class="col-md-12">
            <h4></h4>
            <div class="box box-warning">
                <div class="box-header">
                    @if ($stat->id_status ==     3 || $stat->id_status == 4)
{{--                        <a href="/kaprodi/tambahRka/{{$id_daftar}}">--}}
{{--                            <button type="button" class="btn btn-primary">Tambah RKA</button>--}}
{{--                        </a>--}}
                    @endif
                </div>
                <div class="box-body">
{{--                    <div class="alert alert-success">--}}
{{--                        RKA telah di hapus!--}}
{{--                    </div>--}}
                    @if ($stat->id_status == 1)
                        <div class="row">
                            <div class="col-md-9">

                            </div>
                            <div class="col-md-3">
                                <a href="/rka/pdf/{{$id_daftar}}">
                                    <button class="float-right btn btn-primary">Dowload document</button>
                                </a>
                            </div>
                        </div>
                    @endif
                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
{{--                    @if ($stat->id_status == 3 || $stat->id_status == 4)--}}
                        <p><b>Status : {{$status->status}}</b></p>
{{--                    @endif--}}

                    <table class="table table-hover" id="table-datatables">
                        <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Mata Anggaran</th>
                            <th scope="col">Rincian Program</th>
                            <th scope="col">Vol</th>
                            <th scope="col">Satuan</th>
                            <th scope="col">Harga Satuan</th>
                            <th scope="col">Jumlah</th>
                            @if ($stat->id_status == 3 || $stat->id_status == 4)
                                <th scope="col">Komentar</th>
                                <th scope="col"></th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($rkas as $rka)
                            <tr>
                                <td>{{$a++}}</td>
                                <td>
                                    @if ($daftarRka->id_status == 1)
                                        <a href="/kaprodi/kelolaDana/{{$rka->id_anggaran}}">{{$rka->mata_anggaran}}</a>
                                    @else
                                        {{$rka->mata_anggaran}}
                                    @endif
                                </td>
                                <td>{{$rka->rincian_program}}</td>
                                <td>{{$rka->volume}}</td>
                                <td>{{$rka->satuan}}</td>
                                <td>{{$rka->harga_persatuan}}</td>
                                <td>{{$rka->volume*$rka->harga_persatuan}}</td>
                                @if ($stat->id_status == 3 || $stat->id_status == 4)
                                    <td>
                                        @if (Komentar::where('id_anggaran', '=', $rka->id_anggaran)->exists())
                                            {{Komentar::where('id_anggaran', '=', $rka->id_anggaran)->latest()->first()->komentar}}
                                        @endif
                                    </td>
                                    <td>
                                        <a href="/kaprodi/editrka/{{$rka->id_anggaran}}">
                                            <button type="button" class="btn btn-primary">Edit</button>
                                        </a>
                                        <a href="/kaprodi/hapusrka/{{$rka->id_anggaran}}">
                                            <button type="button" class="btn btn-danger" onclick="return confirm('Apakah anda ingin menghapus RKA ini?');">Hapus</button>
                                        </a>
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                        @if ($stat->id_status == 3 || $stat->id_status == 4)
                            <a href="/kaprodi/kirim/{{$id_daftar}}">
                                <button class="float-right btn btn-secondary">Kirim</button>
                            </a>
                        @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection

