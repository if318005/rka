@extends('dashboard.layouts.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h4></h4>
            <div class="box box-warning">
                <div class="box-header">
                    <h4>Tambah Daftar RKA</h4>
                </div>
                <div class="box-body">
                    <form class="form-horizontal" method="POST" action="/kaprodi/tambahDaftarRka/">
                        {{ csrf_field() }}
                        <input type="hidden" name="tahun" value="{{$tahun}}">
                        <div class="form-group">
                            <label for="nama_Daftar" class="col-md-4 control-label">Nama Daftar</label>

                            <div class="col-md-6">
                                <input id="nama_Daftar" type="text" class="form-control" name="nama_Daftar" required>

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Tambah Daftar RKA
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection
