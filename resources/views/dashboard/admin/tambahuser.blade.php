@extends('dashboard.layouts.master')
<?php
$baris = 1;
?>
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h4></h4>
            <div class="box box-warning">
                <div class="box-header">
                    <h4>Registrasi User</h4>
                </div>
                <div class="box-body">
                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    <form class="form-horizontal" method="POST" action="{{ route('tambahUser') }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="nama" class="col-md-4 control-label">Nama</label>

                            <div class="col-md-6">
                                <input id="nama" type="text" class="form-control" name="nama" required>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="username" class="col-md-4 control-label">Username</label>

                            <div class="col-md-6">
                                <input id="username" type="text" class="form-control" name="username" required>

                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Email</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control" name="email" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="new-password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="new-password" type="password" class="form-control" name="new-password" required>

{{--                                @if ($errors->has('new-password'))--}}
{{--                                    <span class="help-block">--}}
{{--                                        <strong>{{ $errors->first('new-password') }}</strong>--}}
{{--                                    </span>--}}
{{--                                @endif--}}
                            </div>
                        </div>

{{--                        <div class="form-group">--}}
{{--                            <label for="new-password-confirm" class="col-md-4 control-label">Ulang Password</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="new-password-confirm" type="password" class="form-control" name="new-password_confirmation" required>--}}
{{--                            </div>--}}
{{--                        </div>--}}

                        <div class="form-group">
                            <label for="role" class="col-md-4 control-label">Role</label>

                            <div class="col-md-6">
                                <select class="form-control" name="role" id="role">
                                    <option value="2">Dosen</option>
                                    <option value="3">Kepala Prodi</option>
                                    <option value="4">Wakil Rektor</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="prodi" class="col-md-4 control-label">Prodi</label>

                            <div class="col-md-6">
                                <select class="form-control" name="prodi" id="prodi">
                                    @foreach ($prodis as $prodi)
                                        <option value="{{$prodi->id_prodi}}">{{$prodi->nama_prodi}}</option>
                                    @endforeach
                                    <option value="1">D3 TI</option>
                                    <option value="2">D3 TK</option>
                                    <option value="3">D4 TI</option>
                                    <option value="4">S1 TI</option>
                                    <option value="5">S1 SI</option>
                                    <option value="6">S1 TE</option>
                                    <option value="7">S1 BP</option>
                                    <option value="8">S1 MR</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-success">
                                    Daftar
                                </button>
                                <button type="button" class="btn btn-primary mr-5" data-toggle="modal" data-target="#importExcel">
                                    IMPORT EXCEL
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form method="post" action="/tambah_user/excel" enctype="multipart/form-data">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
                    </div>
                    <div class="modal-body">

                        {{ csrf_field() }}

                        <label>Pilih file excel</label>
                        <div class="form-group">
                            <input type="file" name="file" required="required" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Import</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')

@endsection
