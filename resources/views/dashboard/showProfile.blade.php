@extends('dashboard.layouts.master')
<?php
use App\User;$users = User::all();
$baris = 1;
?>
@section('content')
    <div class="box">
        <div class="box-header">
            <div class="col-md-7">
                <h4 style="font-size: 25px;">User Account</h4>
            </div>
            <div class="col-md-5">
                <div class="dropdown" >
                    <b class="dropdown-toggle"data-toggle="dropdown">Account Configuration<span class="glyphicon glyphicon-asterisk"></span></b>
                    <ul class="dropdown-menu">
                        <li><a href="/profile/editProfile">Edit Profile</a></li>
                        <li><a href="/profile/editPassword">Edit Password</a></li>
                    </ul>
                </div>
            </div>
        <div class="box-body">
            <div class="col-md-7">
                <div class="row">
                    <div class="col-sm-4">
                        <b style="font-size: 17px;">Username</b>
                    </div>
                    <div class="col-sm-8">
                        : {{Auth::user()->name}}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <b style="font-size: 17px;">Email</b>
                    </div>
                    <div class="col-sm-8">
                        : {{Auth::user()->email}}
                    </div>
                </div>
            </div>
            <div class="col-md-5">

            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection
