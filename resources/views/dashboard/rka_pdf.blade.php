<!DOCTYPE html>
<html>
<head>
    <title>Daftar Rka</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<style type="text/css">
    table tr td,
    table tr th{
        font-size: 9pt;
    }
</style>
<center>
    <h5>{{$judul->nama_rka}}</h5>
</center>
<br>

<table class='table table-bordered'>
    <thead>
    <tr>
        <th>No</th>
        <th>Mata Anggaran</th>
        <th>Rincian Program/ Aktivitas</th>
        <th>Vol</th>
        <th>Satuan</th>
        <th>Harga Satuan</th>
        <th>Jumlah</th>
    </tr>
    </thead>
    <tbody>
    @php $i=1 @endphp
    @foreach($daftarRka as $rka)
        <tr>
            <td>{{ $i++ }}</td>
            <td>{{$rka->mata_anggaran}}</td>
            <td>{{$rka->rincian_program}}</td>
            <td>{{$rka->volume}}</td>
            <td>{{$rka->satuan}}</td>
            <td>{{$rka->harga_persatuan}}</td>
            <td>{{$rka->volume * $rka->harga_persatuan}}</td>
        </tr>
    @endforeach
    </tbody>
</table>

</body>
</html>
