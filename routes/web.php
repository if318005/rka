<?php

use App\Anggaran;
use App\DaftarRka;
use App\Http\Controllers\AdminController;
use App\Notifications\TestNotifikasi;
use App\Penggunaan;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//    return view('welcome');
    return redirect('/login');
});

Auth::routes();
Route::get('/keluar',function (){
    \Auth::logout();
    return redirect('/');
});
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/test1', 'DashboardController@index');

Route::group(['middleware' => 'auth'], function (){
    Route::get('notifikasi/asRead', function (){
        Auth::user()->unreadNotifications->markAsRead();
        return redirect()->back();
    });
    Route::get('/admin', function (){
        return 'you are admin';
    })->middleware(['auth', 'auth.admin']);

    Route::get('/test', function (){
        return view('dashboard.test');
    });
    Route::get('/downloadLaporanDana', function (){
        return view('dashboard.downloadUsulanDana');
    });

    Route::get('/profile', 'DashboardController@showProfile');
    Route::get('/profile/editProfile', 'DashboardController@showEditProfile');
    Route::get('/profile/editPassword', 'DashboardController@showEditPassword');
    Route::post('/profile/editPassword', 'DashboardController@editPassword')->name('changePassword');
    Route::get('/rka/pdf/{id}', 'DashboardController@cetakRka');
    Route::get('/usulan/pdf/{id}', 'DashboardController@cetakUsulan');
    Route::get('/edit_user/{id}', 'AdminController@showEditUser');
    Route::post('/edit_user/edit', 'AdminController@editUser')->name('editUser');
    Route::post('/dosen/requestRka', 'DashboardController@requestRka');
    Route::get('/dosen/daftarRequest', 'DashboardController@daftarRequestDosen');

    Route::group(['middleware' => 'auth.admin'], function (){
        Route::get('/tambah_user/', 'AdminController@showTambahUser');
        Route::post('/tambah_user/tambah', 'AdminController@tambahUser')->name('tambahUser');
        Route::post('/tambah_user/excel', 'AdminController@tambahUserExcel');
        Route::post('/coba',function (Request $request){
            echo $request->get('role');
        });
        Route::get('/hapus_user/{id}', 'AdminController@hapusUser');
    });

    Route::group(['middleware' => 'auth.wr'], function (){
        Route::get('/wr/rka/{id}', 'WrController@rka');
        Route::get('/wr/terima/{id}', 'WrController@terima');
        Route::get('/wr/tolak/{id}', 'WrController@tolak');
        Route::get('wr/daftarUsulan', function (){
            return view('dashboard.wr.daftarUsulan');
        });
        Route::get('/wr/tolak/{id}', function ($id){
            $daftarRka = DaftarRka::where('id_daftar', '=', $id)->first();
           return view('dashboard.wr.komentar',['daftarRka' => $daftarRka]);
        });
        Route::post('/wr/komentar', 'WrController@komentar');
        Route::get('/wr/daftarUsulan/{id}', 'WrController@lihatUsulan');
        Route::get('/wr/usulan/terima/{id}', 'WrController@terimaUsulan');
        Route::get('/wr/usulan/tolak/{id}', 'WrController@tolakUsulan');
        Route::get('/wr/daftarRka', 'WrController@cariRka');
        Route::post('/wr/daftarRka', 'WrController@daftarRka');
        Route::get('/wr/daftarDana', 'WrController@cariDaftarDana');
        Route::post('/wr/daftarDana', 'WrController@daftarDana');
        Route::get('/wr/laporanDana/{id}', 'WrController@lihatLaporan');
    });

    Route::group(['middleware' => 'auth.kaprodi'], function (){
        Route::get('/kaprodi/daftarRka', 'KaprodiController@daftarRka');
        Route::post('/kaprodi/daftarRka', 'KaprodiController@lihatDaftarRka');
        Route::get('/kaprodi/tambahDaftarRka/{id}', 'KaprodiController@lihatTambahDaftarRka');
        Route::post('/kaprodi/tambahDaftarRka/', 'KaprodiController@tambahDaftarRka');
        Route::get('/kaprodi/daftarrka/{id}', 'KaprodiController@daftarRka2');
        Route::get('/kaprodi/tambahRka/','KaprodiController@lihatTambahRka');
        Route::post('/kaprodi/tambahRka', 'KaprodiController@tambahRka');
        Route::get('/kaprodi/kirim/{id_daftar}', 'KaprodiController@kirim');
        Route::get('/kaprodi/editrka/{id}', 'KaprodiController@showEditRka');
        Route::post('/kaprodi/editrka','KaprodiController@editRka');
        Route::get('/kaprodi/hapusrka/{id}', 'KaprodiController@hapusRka');
        Route::get('/kaprodi/requestDana/{id}', 'KaprodiController@lihatRequestDana');
        Route::post('/kaprodi/requestDana', 'KaprodiController@requestDana');
        Route::get('/kaprodi/hapusUsulan/{id}', 'KaprodiController@hapusUsulan');
        Route::get('/kaprodi/daftarDana', 'KaprodiController@cariDaftarDana');
        Route::post('/kaprodi/daftarDana', 'KaprodiController@daftarDana');
        Route::get('/kaprodi/laporanDana/{id}', 'KaprodiController@lihatLaporan');
//        Route::get('/kaprodi/requestDana', function (){
//            return view('dashboard.kaprodi.requestdana');
//        });
        Route::get('/kaprodi/kelolaDana/{id}', 'KaprodiController@kelolaDana');
        Route::get('/kaprodi/editUsulan/{id}', 'KaprodiController@lihatEditUsulan');
        Route::post('/kaprodi/editUsulan', 'KaprodiController@editUsulan');
        Route::get('/kaprodi/updateRealisasi/{id}', 'KaprodiController@lihatUpdateRealisasi');
        Route::post('/kaprodi/updateRealisasi', 'KaprodiController@updateRealisasi');
        Route::get('/kaprodi/lihatRequest', 'KaprodiController@lihatRequest');
        Route::get('/kaprodi/lihatRequest/terima/{id}', 'KaprodiController@isiRequestTerima');
        Route::post('/kaprodi/lihatRequest/terima', 'KaprodiController@requestTerima');
        Route::get('/kaprodi/lihatRequest/tolak/{id}', 'KaprodiController@isiRequestTolak');
        Route::post('/kaprodi/lihatRequest/tolak', 'KaprodiController@requestTolak');
        Route::get('/kaprodi/editDana', function () {
            return view('dashboard.kaprodi.editDana');
        });
    });

});

Route::get('/cobaText', function (){
    $anggaran = Anggaran::find(2);
    $penggunaans = Penggunaan::where('id_anggaran', '=', $anggaran->id_anggaran)->get();
    return view('coba',['anggaran' => $anggaran, 'penggunaans' => $penggunaans]);
});

Route::post('/cobaText', function (Request $request){
    $file = $request->file('file');
    $tanggal = $request->get('tanggal');
    $rencana_biaya = $request->get('bola');
    $tujuan_file = 'FileUsulan';

    $nama_file = time().'_'.$file->getClientOriginalName();
    $file->move($tujuan_file, $nama_file);

    Penggunaan::create([
        'id_anggaran' => 2,
        'rencana_biaya' => $rencana_biaya,
        'tanggal' => $tanggal,
        'file_usulan' => $nama_file,
        'status' => 2,
    ]);

    return redirect()->back();
});

Route::get('/downloadUsulan/{namaFile}', function ($namaFile) {

});

Route::get('/cobaNotifikasi', function (){
//    $user = \App\User::find(1);
    $user = Auth::user()->notifications;
//
//    $details = [
//        'greeting' => 'Hi Artisan',
//        'body' => 'This is our example notification tutorial',
//        'thanks' => 'Thank you for visiting codechief.org!',
//    ];
//
//    $user->notify(new TestNotifikasi($details));
//
//    return dd("Done");
    foreach ($user as $notification){
        echo $notification->data['data'];
    }
});

