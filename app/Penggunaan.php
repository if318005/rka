<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penggunaan extends Model
{
    protected $table = 'penggunaan';
    protected $primaryKey = 'id_penggunaan';

    protected $fillable = ['id_anggaran', 'rencana_biaya', 'tanggal', 'file_usulan', 'status', 'penanggung_jawab'];

    public function anggaran(){
        return $this->belongsTo(Anggaran::class);
    }
}
