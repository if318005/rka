<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DaftarRka extends Model
{
    protected $table = 'daftar_rka';
    protected $primaryKey = 'id_daftar';
    public function prodi(){
        return $this->belongsTo(Prodi::class);
    }

    public function tahun(){
        return $this->belongsTo(Tahun::class,);
    }

    public function status(){
        return $this->belongsTo(Status::class);
    }

    public function anggarans(){
        return $this->hasMany(Anggaran::class);
    }

    public function usulans(){
        return $this->hasMany(Usulan::class);
    }
}
