<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tahun extends Model
{
    protected $table = 'tahun';
    protected $primaryKey = 'id_Tahun';

    public function daftarRkas(){
        return $this->hasMany(DaftarRka::class);
    }

}
