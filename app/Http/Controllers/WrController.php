<?php

namespace App\Http\Controllers;

use App\Anggaran;
use App\DaftarRka;
use App\Komentar;
use App\Notifications\UsulanNotifikasi;
use App\Penggunaan;
use App\Prodi;
use App\Tahun;
use App\User;
use Illuminate\Http\Request;

class WrController extends Controller
{
    public function rka($id){
        $daftarRka = DaftarRka::where('id_daftar', '=', $id)->first();
        return view('dashboard.wr.rka',['daftarRka' => $daftarRka]);
    }

    public function terima($id){
        $daftarRka = DaftarRka::where('id_daftar', '=', $id)->first();
        $daftarRka->id_status = 1;
        $daftarRka->save();
        $kaprodis = User::where('id_prodi', '=', $daftarRka->id_prodi)->where('id_role', '=', '3')->get();
        $pesan = [
            'pesan' => $daftarRka->nama_rka . " di terima.",
            'url' => '/kaprodi/daftarrka/'.$daftarRka->id_daftar,
        ];
        foreach ($kaprodis as $kaprodi){
            $kaprodi->notify(new UsulanNotifikasi($pesan));
        }

        return redirect('/wr/rka/' . $id)->with("success","Daftar rka di setujui!");
    }

    public function komentar(Request $request){
        $a = 0;
        foreach ($request->get('komentar') as $komentar){
            if (!$komentar == null){
                Komentar::create([
                    'komentar' => $komentar,
                    'id_anggaran' => $request->get('id_anggaran')[$a]
                ]);
            }
            $a++;
        }
        $daftarRka = DaftarRka::find($request->get('id_daftar'));
        $daftarRka->id_status = 3;
        $daftarRka->save();
        $kaprodis = User::where('id_prodi', '=', $daftarRka->id_prodi)->where('id_role', '=', '3')->get();
        $pesan = [
            'pesan' => $daftarRka->nama_rka . " di tolak.",
            'url' => '/kaprodi/daftarrka/'.$daftarRka->id_daftar,
        ];
        foreach ($kaprodis as $kaprodi){
            $kaprodi->notify(new UsulanNotifikasi($pesan));
        }
        return redirect('/wr/rka/' . $request->get('id_daftar'))->with('success', 'Daftar RKA di tolak!');
    }

    public function lihatUsulan($id){
        $anggaran = Anggaran::find($id);
        $penggunaans = Penggunaan::where('id_anggaran', '=', $anggaran->id_anggaran)->latest()->get();
        return view('dashboard.wr.daftarUsulan', ['anggaran' => $anggaran, 'penggunaans' => $penggunaans]);
    }

    public function terimaUsulan($id){
        $penggunaan = Penggunaan::find($id);
        $penggunaan->status = 1;
        $penggunaan->save();
        return redirect()->back()->with('success', 'Usulan di terima!');
    }

    public function tolakUsulan($id){
        $penggunaan = Penggunaan::find($id);
        $penggunaan->status = 3;
        $penggunaan->save();
        return redirect()->back()->with('success', 'Usulan di tolak!');
    }

    public function cariRka(){
        return view('dashboard.wr.cariRka');
    }

    public function daftarRka(Request $request){
        $daftarRka = DaftarRka::where('id_prodi', '=', $request->get('prodi'))->where('id_tahun', '=', $request->get('tahun'));
        if (!$daftarRka->exists()){
            return redirect()->back()->with('error', Prodi::find($request->get('prodi'))->nama_prodi . ' belum mempunyai Daftar RKA tahun '.
            Tahun::find($request->get('tahun'))->tahun);
        }else{
            return redirect('/wr/rka/'.$daftarRka->first()->id_daftar);
        }
    }

    public function cariDaftarDana(){
        return view('dashboard.wr.cariDaftarDana');
    }

    public function daftarDana(Request $request){
        $daftarRka = DaftarRka::where('id_prodi', '=', $request->get('prodi'))->where('id_tahun', '=', $request->get('tahun'));
        if (!$daftarRka->exists()){
            return redirect()->back()->with('error', Prodi::find($request->get('prodi'))->nama_prodi . ' belum mempunyai Daftar Dana tahun '.
                Tahun::find($request->get('tahun'))->tahun);
        }else{
            if (!Anggaran::where('id_daftar', '=', $daftarRka->first()->id_daftar)->exists()){
                return redirect()->back()->with('error', Prodi::find($request->get('prodi'))->nama_prodi . ' belum mempunyai Daftar Dana tahun '.
                    Tahun::find($request->get('tahun'))->tahun);
            }else{
                return redirect('/wr/laporanDana/'.$daftarRka->first()->id_daftar);
            }
        }
    }

    public function lihatLaporan($id){
        $daftarRka = DaftarRka::find($id);
        $judul = 'Laporan Dana ' . Prodi::find($daftarRka->id_prodi)->nama_prodi . 'Tahun ' . Tahun::find($daftarRka->id_tahun)->tahun;
        $anggarans = Anggaran::where('id_daftar', '=', $id)->get();
        $totalDana = 0;
        $realisasi = array();
        foreach ($anggarans as $anggaran){
            $totalDana = $totalDana + ($anggaran->harga_persatuan * $anggaran->volume);
            $realisasi[] = Penggunaan::where('id_anggaran', '=', $anggaran->id_anggaran)->get()->sum('realisai_biaya');
        }
        $totalRealisasi = array_sum($realisasi);
        $sisa = $totalDana - $totalRealisasi;
        return view('dashboard.wr.downloadUsulanDana', [
            'judul' => $judul, 'anggarans' => $anggarans, 'totalDana' => $totalDana, 'totalRealisasi' => $totalRealisasi,
            'sisa' => $sisa, 'realisasi' => $realisasi, 'id_daftar' => $id
        ]);
    }
}
