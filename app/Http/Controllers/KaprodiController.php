<?php

namespace App\Http\Controllers;

use App\Anggaran;
use App\DaftarRka;
// use App\Notifications\RkaMasukNotifikasi;
use App\Notifications\UsulanNotifikasi;
use App\Penggunaan;
use App\Prodi;
use App\Status;
use App\Tahun;
use App\User;
use App\Usulan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class KaprodiController extends Controller
{
    public function daftarRka(){
        return view('dashboard.kaprodi.daftarRka');
    }

    public function lihatDaftarRka(Request $request){
        $exist = DaftarRka::where('id_prodi', '=', Auth::user()->id_prodi)->where('id_tahun', '=', request()->get('tahun'))->count();
        if ($exist == 0){
            $exist2 = 0;
            $status = Status::where('id_status', '=', 4)->get('status')->first();
            $daftarRka = new DaftarRka();
            $prodi = Prodi::find(Auth::user()->id_prodi)->nama_prodi;
            $tahun = Tahun::find($request->tahun)->tahun;
            $daftarRka->nama_rka = 'Daftar Rencana Kegiatan dan Anggaran (RKA) ' . $prodi . ' Tahun ' . $tahun;
            $daftarRka->id_prodi = Auth::user()->id_prodi;
            $daftarRka->id_tahun = $request->tahun;
            $daftarRka->id_status = 4;
            $daftarRka->save();
        }else{
            $daftarRka = DaftarRka::where('id_prodi', '=', Auth::user()->id_prodi)->where('id_tahun', '=', request()->get('tahun'))->first();
            $status = Status::where('id_status', '=', $daftarRka->id_status)->get('status')->first();
            $exist2 = Anggaran::where('id_daftar', '=', $daftarRka->id_daftar)->count();
        }
        if ($exist2 != 0){
            $rkas = Anggaran::where('id_daftar', '=', $daftarRka->id_daftar)->get();
        }else{
            $rkas = [];
        }
        return view('dashboard.kaprodi.rka',['exists' => $exist,'exists2' => $exist2, 'rkas' => $rkas, 'status' => $status, 'daftarRka' => $daftarRka]);
    }

    public function lihatTambahDaftarRka($id){
        return view('dashboard.kaprodi.tambahDaftarRka', ['tahun' => $id]);
    }

    public function tambahDaftarRka(Request $request){
        $daftarRka = new DaftarRka();
        $daftarRka->nama_rka = $request->get('nama_Daftar');
        $daftarRka->id_prodi = Auth::user()->id_prodi;
        $daftarRka->id_tahun = $request->tahun;
        $daftarRka->id_status = 4;
        $daftarRka->save();

        return redirect('home');
    }

    public function daftarRka2($id){
        $daftarRka = Anggaran::where('id_daftar', '=', $id)->get();
        $bla = DaftarRka::where('id_daftar', '=', $id)->first();
        return view('dashboard.kaprodi.daftarRka2', ['rkas' => $daftarRka, 'id_daftar' => $id, 'daftarRka' => $bla]);
    }

    public function lihatTambahRka(){
        $daftarRka = DaftarRka::whereIn('id_status', [3, 4])->where('id_prodi', '=', Auth::user()->id_prodi)->get();
        return view('dashboard.kaprodi.tambahRka',['daftarRkas' => $daftarRka]);
    }

    public function tambahRka(Request $request){
        $anggaran = new Anggaran();
        $anggaran->id_daftar = $request->get('id_daftar');
        $anggaran->mata_anggaran = $request->get('mata_anggaran');
        $anggaran->rincian_program = $request->get('rincian_program');
        $anggaran->volume = $request->get('volume');
        $anggaran->satuan = $request->get('satuan');
        $anggaran->harga_persatuan = $request->get('harga');
        $anggaran->save();

//        $daftarRka = Anggaran::where('id_daftar', '=', ('id_daftar'))->get();
        return redirect('kaprodi/daftarrka/' . $request->get('id_daftar'))->with("success","RKA berhasil ditambahkan!");
    }

    public function hapusRka($id){
        $id_daftar = Anggaran::find($id)->id_daftar;
        $rka = Anggaran::find($id);
        $rka->delete();
        return redirect('/kaprodi/daftarrka/'.$id_daftar)->with('success', 'Rka telah di hapus.');
    }

    public function kirim($id_daftar){
        $daftarRka = DaftarRka::get()->where('id_daftar', '=',$id_daftar)->first();
        $daftarRka->id_status = 2;
        $daftarRka->save();

        $prodi = Prodi::find(Auth::user()->id_prodi);
        $pesan = [
            'pesan' => 'Daftar RKA baru dari ' . $prodi->nama_prodi,
            'url' => 'wr/rka/'.$id_daftar,
        ];

        $wrs = User::where('id_role', '=', 4)->get();
        foreach ($wrs as $wr){
            $wr->notify(new UsulanNotifikasi($pesan));
        }
//
        return redirect()->back()->with("success","Daftar RKA telah dikirim!");
    }

    public function showEditRka($id){
        $rkas = Anggaran::where('id_anggaran', '=', $id)->first();
        return view('dashboard.kaprodi.editRka',['rkas' => $rkas]);
    }

    public function editRka(Request $request){
        $anggaran = Anggaran::where('id_anggaran', '=', $request->get('id_anggaran'))->first();
        $anggaran->mata_anggaran = $request->get('mata_anggaran');
        $anggaran->rincian_program = $request->get('rincian_program');
        $anggaran->volume = $request->get('volume');
        $anggaran->satuan = $request->get('satuan');
        $anggaran->harga_persatuan = $request->get('harga');
        $anggaran->save();

        return redirect('/kaprodi/daftarrka/'.$anggaran->id_daftar)->with("success","RKA berhasil di ubah!");
    }

    public function kelolaDana($id){
        $anggaran = Anggaran::find($id);
        $penggunaans = Penggunaan::where('id_anggaran', '=', $id)->paginate(10);
//        echo $penggunaans;
        return view('dashboard.kaprodi.keloladana', ['anggaran' => $anggaran, 'penggunaans' => $penggunaans]);
    }

    public function lihatRequestDana($id){
        $anggaran = Anggaran::find($id);
        return view('dashboard.kaprodi.requestdana', ['anggaran' => $anggaran]);
    }

    public function requestDana(Request $request){
        $file = $request->file('bukti');
        $tanggal = $request->get('tanggal');
        $rencana_biaya = $request->get('jumlah');
        $tujuan_file = 'FileUsulan';
        $jumlah = 0;
        $blas = Penggunaan::where('id_anggaran', '=', $request->get('id_anggaran'))->get();
        foreach ($blas as $bla){
            $jumlah = $jumlah + $bla->rencana_biaya;
        }
        $maksimal = Anggaran::find($request->get('id_anggaran'))->harga_persatuan * Anggaran::find($request->get('id_anggaran'))->volume;
        if ($maksimal < $jumlah+$rencana_biaya){
            return redirect()->back()->with('error', 'biaya terlalu besar');
        }
        $nama_file = time().'_'.$file->getClientOriginalName();
        $file->move($tujuan_file, $nama_file);

        Penggunaan::create([
            'id_anggaran' => $request->get('id_anggaran'),
            'rencana_biaya' => $rencana_biaya,
            'tanggal' => $tanggal,
            'file_usulan' => $nama_file,
            'status' => 2,
            'penanggung_jawab' => $request->get('penanggung_jawab'),
        ]);
        $prodi = Prodi::find(Auth::user()->id_prodi);
        $pesan = [
            'pesan' => 'Usulan dana baru dari ' . $prodi->nama_prodi,
            'url' => '#',
        ];
        $wrs = User::where('id_role', '=', 4)->get();
        foreach ($wrs as $wr){
            $wr->notify(new UsulanNotifikasi($pesan));
        }

        return redirect('/kaprodi/kelolaDana/'.$request->get('id_anggaran'))->with('success', 'Usulan berhasil di tambahkan!');
    }

    public function hapusUsulan($id){
        $usulan = Penggunaan::find($id);
        $usulan->delete();
        return redirect()->back()->with('success', 'Usulan telah di hapus!');
    }

    public function cariDaftarDana(){
        return view('dashboard.kaprodi.cariDaftarDana');
    }

    public function daftarDana(Request $request){
        $daftarRka = DaftarRka::where('id_prodi', '=', Auth::user()->id_prodi)->where('id_tahun', '=', $request->get('tahun'));
        if (!$daftarRka->exists()){
            return redirect()->back()->with('error', ' belum mempunyai Daftar Dana tahun '. Tahun::find($request->get('tahun'))->tahun);
        }else{
            if (!Anggaran::where('id_daftar', '=', $daftarRka->first()->id_daftar)->exists()){
                return redirect()->back()->with('error', ' belum mempunyai Daftar Dana tahun '. Tahun::find($request->get('tahun'))->tahun);
            }else{
                return redirect('/kaprodi/laporanDana/'.$daftarRka->first()->id_daftar);
            }
        }
    }

    public function lihatLaporan($id){
        $daftarRka = DaftarRka::find($id);
        $judul = 'Laporan Dana ' . Prodi::find($daftarRka->id_prodi)->nama_prodi . 'Tahun ' . Tahun::find($daftarRka->id_tahun)->tahun;
        $anggarans = Anggaran::where('id_daftar', '=', $id)->get();
        $totalDana = 0;
        $realisasi = array();
        foreach ($anggarans as $anggaran){
            $totalDana = $totalDana + ($anggaran->harga_persatuan * $anggaran->volume);
            $realisasi[] = Penggunaan::where('id_anggaran', '=', $anggaran->id_anggaran)->get()->sum('realisai_biaya');
        }
        $totalRealisasi = array_sum($realisasi);
        $sisa = $totalDana - $totalRealisasi;
        return view('dashboard.kaprodi.downloadUsulanDana', [
            'judul' => $judul, 'anggarans' => $anggarans, 'totalDana' => $totalDana, 'totalRealisasi' => $totalRealisasi,
            'sisa' => $sisa, 'realisasi' => $realisasi, 'id_daftar' => $id, 'daftarRka' => $daftarRka
        ]);
    }

    public function lihatEditUsulan($id){
        $penggunaan = Penggunaan::find($id);
        $anggaran = Anggaran::find($penggunaan->id_anggaran);
        return view('dashboard.kaprodi.editUsulan', ['penggunaan' => $penggunaan, 'anggaran' => $anggaran]);
    }

    public function editUsulan(Request $request){
        $file = $request->file('bukti');
        $tanggal = $request->get('tanggal');
        $rencana_biaya = $request->get('jumlah');
        $tujuan_file = 'FileUsulan';
        $jumlah = 0;
        $blas = Penggunaan::where('id_anggaran', '=', $request->get('id_anggaran'))->get();
        foreach ($blas as $bla){
            $jumlah = $jumlah + $bla->rencana_biaya;
        }
        $maksimal = Anggaran::find($request->get('id_anggaran'))->harga_persatuan * Anggaran::find($request->get('id_anggaran'))->volume;
        if ($maksimal < $jumlah+$rencana_biaya){
            return redirect()->back()->with('error', 'biaya terlalu besar');
        }
        $nama_file = time().'_'.$file->getClientOriginalName();
        $file->move($tujuan_file, $nama_file);

        $penggunaan = Penggunaan::find($request->get('id_penggunaan'));
        $penggunaan->rencana_biaya = $rencana_biaya;
        $penggunaan->tanggal = $tanggal;
        $penggunaan->file_usulan = $nama_file;
        if ($penggunaan->status == 3) {
            $penggunaan->status = 2;

            $prodi = Prodi::find(Auth::user()->id_prodi);
            $pesan = [
                'pesan' => 'Usulan dana baru dari ' . $prodi->nama_prodi,
                'url' => '#',
            ];
            $wrs = User::where('id_role', '=', 4)->get();
            foreach ($wrs as $wr) {
                $wr->notify(new UsulanNotifikasi($pesan));
            }
        }
        $penggunaan->save();

        return redirect('kaprodi/kelolaDana/'.$request->get('id_anggaran'))->with('success', 'Usulan telah di ubah!');
    }

    public function lihatUpdateRealisasi($id){
        $penggunaan = Penggunaan::find($id);
        $anggaran = Anggaran::find($penggunaan->id_anggaran);
        return view('dashboard.kaprodi.uploadRealisasi', ['penggunaan' => $penggunaan, 'anggaran' => $anggaran]);
    }

    public function updateRealisasi(Request $request){
        $file = $request->file('bukti');
        $realisasi = $request->get('realisasi');
        $tujuan_file = 'FileRealisasi';


        $nama_file = time().'_'.$file->getClientOriginalName();
        $file->move($tujuan_file, $nama_file);

        $penggunaan = Penggunaan::find($request->get('id_penggunaan'));
        $penggunaan->realisai_biaya = $realisasi;
        $penggunaan->file_realisasi = $nama_file;
        $penggunaan->status = 4;
        $penggunaan->save();

        return redirect('/kaprodi/kelolaDana/'.$request->get('id_anggaran'))->with('success', 'Realisasi telah ditambahkan!');
    }

    public function lihatRequest(){
        $usulans = [];
        $daftarrkas = DaftarRka::where('id_prodi', '=', Auth::user()->id_prodi)->get();
        foreach ($daftarrkas as $daftarrka){
            $blas = Usulan::where('id_daftar', '=', $daftarrka->id_daftar)->get();
            foreach ($blas as $bla){
                $usulans[] = $bla;
            }
        }
//        foreach ($usulans as $usulan){
//            echo $usulan;
//        }
        return view('dashboard.kaprodi.lihatRequestDosen', ['usulans' => $usulans]);
    }

    public function isiRequestTerima($id){
        $usulan = Usulan::find($id);
        return view('dashboard.kaprodi.requestDosenTerima', ['usulan' => $usulan]);
    }

    public function requestTerima(Request $request){
        $Anggaran = new Anggaran();
        $Anggaran->id_daftar = $request->get('id_daftar');
        $Anggaran->mata_anggaran = $request->get('mata_anggaran');
        $Anggaran->rincian_program = $request->get('rincian_program');
        $Anggaran->volume = $request->get('volume');
        $Anggaran->satuan = $request->get('satuan');
        $Anggaran->harga_persatuan = $request->get('harga');
        $Anggaran->save();

        $Usulan = Usulan::find($request->get('id_usulan'));
        $Usulan->status_usulan = 1;
        $Usulan->save();

        return redirect('/kaprodi/lihatRequest')->with('success', 'Request di terima!');
    }

    public function isiRequestTolak($id){
        $usulan = Usulan::find($id);
        return view('dashboard.kaprodi.requestDosenTolak', ['usulan' => $usulan]);
    }

    public function requestTolak(Request $request){

        $Usulan = Usulan::find($request->get('id_usulan'));
        $Usulan->status_usulan = 3;
        $Usulan->komentar_usulan = $request->get('komentar');
        $Usulan->save();
        return redirect('/kaprodi/lihatRequest')->with('success', 'Request di tolak!');
    }
}
