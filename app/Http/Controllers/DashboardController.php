<?php

namespace App\Http\Controllers;

use App\Anggaran;
use App\DaftarRka;
use App\Notifications\UsulanNotifikasi;
use App\Penggunaan;
use App\Prodi;
use App\Tahun;
use App\User;
use App\Usulan;
use Barryvdh\DomPDF\Facade as PDF;
use Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index(){
//        if (Auth::user()->id_role == 1){
            return view('dashboard.home');
//        }
    }

    public function cetakRka($id){
        $daftarRka = Anggaran::where('id_daftar', '=', $id)->get();
        $judul = DaftarRka::where('id_daftar', '=', $id)->get('nama_rka')->first();
        $pdf = PDF::loadview('dashboard.rka_pdf',['daftarRka' => $daftarRka, 'judul' => $judul]);
        return $pdf->download('laporan-rka.pdf');
    }

    public function cetakUsulan($id){
        $daftarRka = DaftarRka::find($id);
        $judul = 'Laporan Dana ' . Prodi::find($daftarRka->id_prodi)->nama_prodi . 'Tahun ' . Tahun::find($daftarRka->id_tahun)->tahun;
        $anggarans = Anggaran::where('id_daftar', '=', $id)->get();
        $totalDana = 0;
        $realisasi = array();
        foreach ($anggarans as $anggaran){
            $totalDana = $totalDana + ($anggaran->harga_persatuan * $anggaran->volume);
            $realisasi[] = Penggunaan::where('id_anggaran', '=', $anggaran->id_anggaran)->get()->sum('realisai_biaya');
        }
        $totalRealisasi = array_sum($realisasi);
        $sisa = $totalDana - $totalRealisasi;
        $pdf = PDF::loadview('dashboard.downloadUsulanDana', ['judul' => $judul, 'anggarans' => $anggarans, 'totalDana' => $totalDana, 'totalRealisasi' => $totalRealisasi,
            'sisa' => $sisa, 'realisasi' => $realisasi, 'id_daftar' => $id]);
        return $pdf->download('laporan-usulan.pdf');
    }

    public function showProfile(){
        return view('dashboard.showProfile');
    }

    public function showEditPassword(){
        return view('dashboard.editPassword');
    }

    public function editPassword(Request $request){
        $user = User::where('id', '=', Auth::user()->id)->first();
        if (!Hash::check($request->get('current-password'), $user->password)) {

            // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }

        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }

        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);

        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();

        return redirect()->back()->with("success","Password changed successfully !");
    }

    public function requestRka(Request $request){
        $kaprodis = User::where('id_prodi', '=', Auth::user()->id_prodi)->where('id_role', '=', 3)->get();
        $usulan = new Usulan();
        $usulan->id_daftar = $request->get('id_daftar');
        $usulan->rincianprogram = $request->get('deskripsi');
        $usulan->volume_usulan = $request->get('volume');
        $usulan->satuan_usulan = $request->get('satuan');
        $usulan->hargapersatuan = $request->get('harga');
        $usulan->keterangan = $request->get('keterangan');
        $usulan->status_usulan = 2;
        $usulan->save();

        $pesan = [
            'pesan' => Auth::user()->username . " request RKA",
            'url' => '/kaprodi/lihatRequest',
        ];
        foreach ($kaprodis as $kaprodi){
            $kaprodi->notify(new UsulanNotifikasi($pesan));
        }
        return redirect()->back()->with('success', 'Rka telah di request!');
    }

    public function daftarRequestDosen(){
        $Usulan = Usulan::latest()->paginate(10);
        return view('dashboard.dosen.daftarRequestDosen', ['usulans' => $Usulan]);
    }
}
