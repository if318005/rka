<?php

namespace App\Http\Controllers;

use App\Imports\UserImport;
use App\Prodi;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class AdminController extends Controller
{
    public function showTambahUser(){
        $prodi = Prodi::all();
        return view('dashboard.admin.tambahuser', ['prodis' => $prodi]);
    }

    public function tambahUser(Request $request){
        if(User::where('username', '=', $request->get('username'))->exists()){
            //Name are exist
            return redirect()->back()->with("error","Username tersebut telah dipakai");
        }

        if(User::where('email', '=', $request->get('email'))->exists()){
            //Name are exist
            return redirect()->back()->with("error","User dengan email tersebut telah ada");
        }

//        if (!strcmp($request->get('current-password'),Auth::user()->password)) {
//            // The passwords matches
//            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
//        }

        $validatedData = $request->validate([
            'username' => 'required',
            'nama' => 'required',
            'email' => 'required',
            'new-password' => 'required|string|min:6',
        ]);

//        User::create([
//            'name' => $request->get('nama'),
//            'id_role' => $request->get('role'),
//            'email' => $request->get('email'),
//            'password' => bcrypt($request->get('new-password')),
//            'id_prodi' => $request->get('new-password')
//        ]);
        $user = new User();
        $user->username = $request->get('username');
        $user->name = $request->get('nama');
        $user->id_role = $request->get('role');
        $user->email = $request->get('email');
        $user->password = bcrypt($request->get('new-password'));
        $user->id_prodi = $request->get('prodi');
        $user->save();

        return redirect()->back()->with("success","User berhasil ditambahkan!");
    }

    public function tambahUserExcel(Request $request){
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        $file = $request->file('file');
        $nama_file = rand().$file->getClientOriginalName();
        $file->move('FileRegistrasi',$nama_file);
        Excel::import(new UserImport(), public_path('/FileRegistrasi/'.$nama_file));
        return redirect()->back()->with('success', 'User behasil di tambahkan');
    }

    public function showEditUser($id){

        return view('dashboard.admin.edituser', ['user' => Auth::user()]);
    }

    public function editUser(Request $request){
        if (!strcmp($request->get('current-password'),Auth::user()->password)) {
            // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }

        $validatedData = $request->validate([
            'username' => 'required',
            'nama' => 'required',
            'email' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);

        //Change Data
        $user = Auth::user();
        $user->username = $request->get('username');
        $user->name = $request->get('nama');
        $user->email = $request->get('email');
        $user->password = bcrypt($request->get('new-password'));
        $user->save();

        return redirect()->back()->with("success","Data changed successfully !");
    }

    public function hapusUser($id){
        $user = User::find($id);
        $user->delete();
        return redirect()->back()->with('success', 'User tersebut telah di hapus');
    }
}
