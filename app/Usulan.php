<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usulan extends Model
{
    protected $table = 'usulan';
    protected $primaryKey = 'id_usulan';

    public function daftarrka(){
        return $this->belongsTo(DaftarRka::class);
    }
}
