<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
    protected $table = 'komentar';
    protected $primaryKey = 'id_komentar';
    protected $fillable = [
        'komentar', 'id_anggaran',
    ];

    public function anggaran(){
        return $this->belongsTo(Anggaran::class);
    }
}
