<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'status';
    protected $primaryKey = 'id_status';

    public function daftarRkas(){
        return $this->hasMany(DaftarRka::class);
    }
}
