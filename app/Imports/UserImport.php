<?php

namespace App\Imports;

use App\User;
use Maatwebsite\Excel\Concerns\ToModel;

class UserImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new User([
            'username' => $row[1],
            'name' => $row[2],
            'email' => $row[3],
            'id_role' => $row[4],
            'password' => bcrypt($row[5]),
            'id_prodi' => $row[6],
        ]);
    }
}
