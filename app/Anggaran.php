<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anggaran extends Model
{
    protected $table = 'anggaran';
    protected $primaryKey = 'id_anggaran';

    public function daftarRka(){
        return $this->belongsTo(DaftarRka::class);
    }

    public function komentars(){
        return $this->belongsTo(Komentar::class);
    }

    public function penggunaans(){
        return $this->hasMany(Penggunaan::class);
    }
}
